package com.example

class Docker implements Serializable  
{
    def script

    Docker(script)
    {
        this.script=script
    }
        
    def dockerLogIn()
    {
        script.withCredentials([script.usernamePassword(credentialsId: 'docker-hub-cred', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) 
        {
            script.sh "echo $script.PASSWORD | docker login -u $script.USERNAME --password-stdin"
        }
    }

    def dockerBuild(String imageName)
    {
        script.sh "docker build -t $imageName ."
    }

    def dockerPush(String imageName)
    {
        script.sh "docker push $imageName" 
    }
}